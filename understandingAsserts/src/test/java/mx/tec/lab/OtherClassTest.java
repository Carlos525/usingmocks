package mx.tec.lab;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class OtherClassTest {
	@Test
	public void testMultiply() {
		OtherClass oc = new OtherClass();
	    oc.multiply(3,4);
	}

	@Test
	public void testMultiply_ExceptionIsThrown() {
		OtherClass ocE = new OtherClass();
		 try {
		    ocE.multiply(1000, 5);
		    fail("Exception not thrown");
		 } catch (IllegalArgumentException e) {
		    assertEquals("X should be less than 1000", e.getMessage());
		 }
	}
}
